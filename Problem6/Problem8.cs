﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edu.unl.cjung.numanl
{
    class Problem8
    {
        static void Main(string[] args)
        {
            SingleServer s = new SingleServer();
            s.Lambda = MeanArrivalTime(); // I feel like this should be 1/MeanArrivalTime, but that gives me a negative wait time
            s.Mu = MeanServiceTime(); // Same here

            Console.WriteLine(String.Format("Average wait time for Single Teller, Single Line: {0}", s.WQ));


            MultiServer m = new MultiServer();
            m.Lambda = MeanArrivalTime();
            m.Mu = MeanServiceTime();
            m.Servers = 2;

            Console.WriteLine(String.Format("Average wait time for 2 Teller, Single Line: {0}", m.WQ));


            Console.ReadKey(); // Wait for input to exit
        }

        private static double MeanArrivalTime()
        {
            double arrivalTimes = 20 + 1 + 1 + 3 + 3 + 1 + 4 + 3 + 1 + 1 + 1 + 1 + 5;
            return arrivalTimes / 13; // 13 = number of customers
        }

        private static double MeanServiceTime()
        {
            double serviceTimes = 5 + 2 + 3 + 4 + 6 + 4 + 3 + 2 + 3 + 4 + 4 + 3 + 4;
            return serviceTimes / 13;
        }
    }
}
