﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edu.unl.cjung.numanl
{
    class SingleServer
    {
        public double Lambda { get; set; }
        public double Mu { get; set; }
        public double P
        {
            get
            {
                return Lambda / Mu;
            }
        }
        public double L
        {
            get
            {
                return Lambda / (Mu - Lambda);
            }
        }
        public double LQ
        {
            get
            {
                return P * L;
            }
        }
        public double W
        {
            get
            {
                return 1 / (Mu - Lambda);
            }
        }
        public double WQ
        {
            get
            {
                return P * W;
            }
        }
    }
}
