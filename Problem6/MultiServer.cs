﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace edu.unl.cjung.numanl
{
    class MultiServer
    {
        public double Lambda { get; set; }
        public double Mu { get; set; }
        public int Servers { get; set; }
        public double P
        {
            get
            {
                return Lambda / (Servers * Mu);
            }
        }
        public double P0
        {
            get
            {
                double sum = 0;
                for (int i = 0; i < Servers; i++)
                {
                    sum += Math.Pow(Lambda / Mu, i) / Factorial(i);
                }

                double eqn = sum + (Math.Pow(Lambda / Mu, Servers) / Factorial(Servers)) * (1 / (1 - P));

                return 1 / eqn;
            }
        }
        public double L
        {
            get
            {
                return Lambda * W;
            }
        }
        public double LQ
        {
            get
            {
                return (P0*Math.Pow(Lambda/Mu, Servers)*P)/(Factorial(Servers)*Math.Pow(1-P, 2));
            }
        }
        public double W
        {
            get
            {
                return WQ + 1 / Mu;
            }
        }
        public double WQ
        {
            get
            {
                return LQ / Lambda;
            }
        }

        private static int Factorial(int num)
        {
            if (num == 0)
                return 1;

            int returnVal = 1;
            for (int i = 1; i <= num; i++)
            {
                returnVal *= i;
            }

            return returnVal;
        }
    }
}
